I welcome clients who are struggling with specific diagnoses as well as those who are just having difficulty with day-to-day stresses or struggles to finding a greater purpose, and develop into the healthiest and most fulfilled version of self. I love to work with a diverse range of individuals who want to make changes, develop coping skills to heal from trauma, and generally lead more satisfying lives.

Address: 2050 Breton Se, Suite 103, Grand Rapids, MI 49546, USA

Phone: 616-835-0215

Website: http://www.counselingwitholja.com
